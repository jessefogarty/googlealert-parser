import feedparser
import time
import sqlite3
import re
import datetime

class Parser():
    '''
    Retrieve a Google Alert feed

    feed : str
        The Google Alert Atom Feed URL. Defaults to Doug Ford search query.
    '''
    def __init__(self):
        # Check for or create new database table
        conn = sqlite3.connect('gfeed.db')
        cur = conn.cursor()
        cur.execute('''CREATE TABLE IF NOT EXISTS `articles` (
            `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
            `title`	TEXT NOT NULL,
            `published`	TEXT NOT NULL,
            `description`	TEXT NOT NULL,
            `link`	TEXT NOT NULL UNIQUE,
            `full_content`  TEXT
            );''')
        conn.commit()
        conn.close()
    def getfeed(self, url):
        '''
        Parse Google Alert feed entries & submit to database.
        '''
        # Get dbrows total to track new row submissions
        conn = sqlite3.connect('gfeed.db')
        cur = conn.cursor()
        dbrows1 = cur.execute('''SELECT COUNT(id) FROM articles''').fetchall()
        dbrows1 = dbrows1[0][0]
        print('Obtaining Google Alert Feed...')
        feed = feedparser.parse(url)
        # create a list of entries to parse
        entryList = feed['entries']
        # setup counter variable and max # of entries for loop
        total = len(entryList)
        count = 0
        print('Parsing Feed Entries...')
        # parse entries and submit to database
        for entry in entryList:
            if count <= total:
                count = count + 1
                print('Parsing Article #', count)
                title = entry['title']
                published = entry['published']
                rawContent = entry.content
                # extract raw description from dict into content
                for value in rawContent:
                    content = value['value']
                # extract raw link from dict into link
                rawLink = entry['links']
                for href in rawLink:
                    link = href['href']
                    link = re.findall('^.+url=(.+)&ct=.+$', link)
                    link = link[0]
            conn = sqlite3.connect('gfeed.db')
            cur = conn.cursor()
            cur.execute('''INSERT OR IGNORE INTO articles (title, published, description, link) VALUES (?, ?, ?, ?)''', (title, published, content, link))
            conn.commit()
            time.sleep(0.15)
        # Get final row count dbrows2 to compare against dbrows1
        dbrows2 = cur.execute('''SELECT COUNT(id) FROM articles''').fetchall()
        dbrows2 = dbrows2[0][0]
        dbfinal = dbrows2 - dbrows1
        print('Finished submitting', dbfinal, 'articles to the database.')
        conn.close()
    def urlcheck(self):
        '''
        Pull all articles from database, check for google.com redirect.

        UPDATE rows with parsed URL or IGNORE if no redirect found.
        '''
        conn = sqlite3.connect('gfeed.db')
        cur = conn.cursor()
        articles = cur.execute('''SELECT * from articles''').fetchall()
        # REMOVE AFTER TEST
        count = 0
        for article in articles:
            # REMOVE AFTER TEST
            count = count + 1
            id = article[0]
            title = article[1]
            pub = article[2]
            desc = article[3]
            link = article[4]
            # Find bad urls or skip
            if re.search('www.google.com', link):
                # string sripped & made normal str var
                link = re.findall('url=(.+$)', link); link = link[0]
                cur.execute('''
                                 UPDATE OR IGNORE articles SET
                                 link=? WHERE id=?''', (link, id))
                conn.commit()
            else:
                break
        conn.close()
    def runlog(self):
        conn = sqlite3.connect('gfeed.db')
        cur = conn.cursor()
        # Get articles
        articles = cur.execute('''SELECT * from articles''').fetchall()
        # Parse outpute variables
        articleTotal = len(articles)
        today = datetime.datetime.today()
        print('Article Total:', articleTotal, '   ', 'Updated:', today, file=open('runlog.txt', 'a'))


# Test Run of Code
if __name__ == "__main__":
    print('Enter the location of your file containing your feed URLs')
    fh = input('File: '); feeds = open(fh, 'r').readlines()
    for feed in feeds:
        run = Parser()
        run.getfeed(feed)
    run.runlog()
